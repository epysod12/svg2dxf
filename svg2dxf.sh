#!/bin/bash

# Paramétrage des couleurs
RED='\033[0;31m'
GRN='\033[0;32m'
YLW='\033[0;33m'
PRL='\033[0;35m'
WHT='\033[0;37m'

# Définition des variables
state=(ouvert fermé)

# Requête auprès de l'utilisateur (permet une interruption momentanée du script)
read -p "$(echo -e ${PRL}Voulez-vous traiter le répertoire courant de manière récursive ?${WHT} [o/N]) " prompt

# Test pour déterminer la méthode de traitement (en fonction du choix de l'utilisateur)
if [[ $prompt = 'o' || $prompt = 'O' || $prompt = 'oui' || $prompt = 'Oui' ]]
then
    svglist=`find . -name \*.svg`
elif [[ $prompt = 'n' || $prompt = 'N' || $prompt = 'non' || $prompt = 'Non' || $prompt = '' ]]
then
    svglist=*.svg
else
    echo -e "${RED}=================${WHT}"
    echo -e "${RED}Choix non valide.${WHT}"
    echo -e "${RED}=================${WHT}"
    exit 0
fi

# Traitement individuel des fichiers SVG
for drawing in $svglist
do

    # Récupération des variables 'dirname' et 'basename'
    PATHNAME=`dirname $drawing`
    FILENAME=`basename $drawing .svg`

    # Affichage du processus en cours
    echo -e "\nTraitement du fichier ${YLW}$drawing${WHT} en cours..."

    # Identification des calques présents dans le fichier SVG
    layerslist=`xmlstarlet select -N ns=http://www.w3.org/2000/svg -t -v "/ns:svg/ns:g[contains(@inkscape:groupmode,'layer')]/@id" $drawing`

    # Création d'un fichier .log pour le fichier SVG traité
    touch $PATHNAME/$FILENAME.log

    # Test pour vérifier si le calque 'layer3' est présent dans le fichier SVG
    if [[ $layerslist =~ 'layer3' ]]
    then

        # Évaluation du nombre de chemins contenus dans le calque 'layer3'
        nbpath=`xmlstarlet select -N ns=http://www.w3.org/2000/svg -t -v "count(/ns:svg/ns:g[contains(@id,'layer3')]/ns:path/@d)" $drawing`

        # Test pour vérifier si le calque 'layer3' contient des chemins
        if [[ $nbpath != 0 ]]
        then

            # Écriture du fichier .log
            echo "Le calque 'layer3' contient $nbpath chemin(s) :" > $PATHNAME/$FILENAME.log

            # Identification des 'id' des chemins contenus dans le calque 'layer3'
            idlist=`xmlstarlet select -N ns=http://www.w3.org/2000/svg -t -v "/ns:svg/ns:g[contains(@id,'layer3')]/ns:path/@id" $drawing`

            # Copie du SVG sur lequel on travaille désormais
            cp $drawing "$PATHNAME/$FILENAME"_tmp.svg

            # Suppression des images présentes dans le fichier SVG temporaire
            xmlstarlet edit --inplace -N ns=http://www.w3.org/2000/svg -d "/ns:svg/ns:g[contains(@inkscape:groupmode,'layer')]/ns:image" "$PATHNAME/$FILENAME"_tmp.svg

            # Traitement individuel des chemins contenus dans le calque 'layer3'
            for item in $idlist
            do

                # Chargement des attributs 'd' et 'nodetypes' du chemin courant
                pathdata=`xmlstarlet select -N ns=http://www.w3.org/2000/svg -t -v "/ns:svg/ns:g[contains(@id,'layer3')]/ns:path[contains(@id,'$item')]/@d" "$PATHNAME/$FILENAME"_tmp.svg`
                nodedata=`xmlstarlet select -N ns=http://www.w3.org/2000/svg -t -v "/ns:svg/ns:g[contains(@id,'layer3')]/ns:path[contains(@id,'$item')]/@sodipodi:nodetypes" "$PATHNAME/$FILENAME"_tmp.svg`

                # Paramétrage par défaut de la variable 'zvalue'
                zvalue=0

                # Test du chemin pour vérifier s'il est fermé par la commande 'closepath'
                if [[ ${pathdata:(-1)} = 'z' || ${pathdata:(-1)} = 'Z' ]]
                then

                    # Suppression de la commande 'closepath'
                    adjust=${pathdata:0:-1}
                    xmlstarlet edit --inplace -N ns=http://www.w3.org/2000/svg -u "/ns:svg/ns:g[contains(@id,'layer3')]/ns:path[contains(@id,'$item')]/@d" -v "$adjust" "$PATHNAME/$FILENAME"_tmp.svg
                    zvalue=1
                fi

                # Lancement d'Inkscape avec l'extension Flatten Bézier
                inkscape --file="$PATHNAME/$FILENAME"_tmp.svg --select=$item --verb=org.ekips.filter.flatten.noprefs --verb=FileSave --verb=FileQuit

                # Test pour déterminer si le chemin était fermé
                if (( $zvalue == 1 ))
                then

                    # Re-chargement de l'attribut 'd'
                    pathdata=`xmlstarlet select -N ns=http://www.w3.org/2000/svg -t -v "/ns:svg/ns:g[contains(@id,'layer3')]/ns:path[contains(@id,'$item')]/@d" "$PATHNAME/$FILENAME"_tmp.svg`

                    # Correction des coordonnées dans l'attribut 'd'
                    newcoord=`echo $pathdata | awk -F'L' '{$NF=""; print $0}'`
                    xmlstarlet edit --inplace -N ns=http://www.w3.org/2000/svg -u "/ns:svg/ns:g[contains(@id,'layer3')]/ns:path[contains(@id,'$item')]/@d" -v "$newcoord" "$PATHNAME/$FILENAME"_tmp.svg
                fi

                # Test pour déterminer la présence de l'attribut 'nodetypes'
                if [[ -n $nodedata ]]
                then

                    # Remplacement des valeurs 's', 'z' et 'a' par la valeur 'c' dans l'attribut 'nodetypes'
                    node_mod=`echo ${nodedata//[sza]/c} | cut -c $((zvalue+1))-${#nodedata}`
                    xmlstarlet edit --inplace -N ns=http://www.w3.org/2000/svg -u "/ns:svg/ns:g[contains(@id,'layer3')]/ns:path[contains(@id,'$item')]/@sodipodi:nodetypes" -v "$node_mod" "$PATHNAME/$FILENAME"_tmp.svg

                    # Évaluation du nombre de points
                    nbnode=`echo -n $node_mod | wc -m`
                else

                    # Absence d'information sur l'attribut 'nodetypes'
                    nbnode='?'
                fi

                # Écriture du fichier .log
                echo "$item ($nbnode pts) -> ${state[$zvalue]}" >> $PATHNAME/$FILENAME.log
            done

            # Export du calque 'layer3' vers un fichier EPS
            inkscape --file="$PATHNAME/$FILENAME"_tmp.svg --export-id='layer3' --export-id-only --export-eps=$PATHNAME/$FILENAME.eps

            # Conversion du fichier EPS vers un fichier DXF
            pstoedit -f dxf $PATHNAME/$FILENAME.eps $PATHNAME/$FILENAME.dxf

            # Suppression du fichier SVG temporaire
            rm "$PATHNAME/$FILENAME"_tmp.svg

            # Suppression du fichier EPS temporaire
            rm $PATHNAME/$FILENAME.eps

            # Affichage du résultat
            echo -e "${GRN}└─> Fichier ${PRL}$PATHNAME/${YLW}$FILENAME.dxf${GRN} créé avec succès !${WHT}"
        else

            # Affichage du message d'erreur
            echo -e "${RED}├─> Erreur détectée !${WHT}"
            echo -e "${RED}├─> Le calque 'layer3' ne contient aucun chemin.${WHT}"
            echo -e "${RED}└─> Aucun fichier DXF créé...${WHT}"

            # Écriture du fichier .log
            echo "Le calque 'layer3' ne contient aucun chemin." > $PATHNAME/$FILENAME.log
        fi
    else

        # Affichage du message d'erreur
        echo -e "${RED}├─> Erreur détectée !${WHT}"
        echo -e "${RED}├─> Le calque 'layer3' n'existe pas.${WHT}"
        echo -e "${RED}└─> Aucun fichier DXF créé...${WHT}"

        # Écriture du fichier .log
        echo "Le calque 'layer3' n'existe pas." > $PATHNAME/$FILENAME.log
    fi
done
