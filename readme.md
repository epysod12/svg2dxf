# svg2dxf.sh

Ce script convertit tous les fichiers SVG d'un répertoire en fichiers DXF.

## Prérequis

  * inkscape
  * ghostscript
  * pstoedit
  * xmlstarlet

## Utilisation

Le script `svg2dxf.sh` doit être placé dans le répertoire qui contient les fichiers SVG à convertir.  
Le lancement s'effectue dans un terminal :

```
$ bash svg2dxf.sh
```

## Fonctionnement

  1. ouverture du fichier SVG et export au format EPS
  2. conversion du fichier EPS vers le format DXF
  3. suppression du fichier EPS

Attention : seul le calque ayant l'id 'layer3' est exporté.

## Licence

Le programme ci-joint est publié sous licence GPLv3.  
[www.gnu.org/licenses/gpl-3.0.fr.html](https://www.gnu.org/licenses/gpl-3.0.fr.html)
